def dbox(width,hight,x0,y0,fx)
  x1 = width + x0
  y1 = (hight + y0)
  pp1 = [x0.mm,-y0.mm,0]
  pp2 = [x1.mm,-y0.mm,0]
  pp3 = [x1.mm,-y1.mm,0]
  pp4 = [x0.mm,-y1.mm,0]  
  entities = Sketchup.active_model.entities
  entities.add_face pp1,pp2,pp3,pp4
  entities.add_dimension_linear pp1,pp2,[0,3,0]
  entities.add_dimension_linear pp1,pp4,[-3,0,0]
  # 线段中点
  lx = x0+width / 2
  ly = y0 + hight / 2
  # 构建 中点坐标
  m1 = [lx.mm,-y0.mm,0]
  m2 = [x1.mm,-ly.mm,0]
  m3 = [lx.mm,-y1.mm,0]
  m4 = [x0.mm,-ly.mm,0]  
  if fx == 's'
    # 上旋
    entities.add_line pp4,m1
    entities.add_line pp3,m1
   end
  if fx == 'y'
    # 右开
    entities.add_line pp4,m2
    entities.add_line pp1,m2
  end        
  if fx == 'z'
    # 左开
    entities.add_line pp2,m4
    entities.add_line pp3,m4
end    
  if fx == 'x'
    # 下璇
    entities.add_line pp1,m3
    entities.add_line pp2,m3
  end  
  
  return 1;
end

require 'csv'

CSV.open("a1.csv","r").each do |person|  
    puts person.inspect
    puts person[0]
    dbox(person[3].to_i,person[4].to_i,person[5].to_i,person[6].to_i,person[7])
    #dbox(550,800,0,0,'x')
end  


#dbox(550,800,0,0,'x')
#dbox(550,800,0,800,'z')
#dbox(550,800,0,1600,'y')
#dbox(550,800,550,0,0)
#dbox(550,800,550,800,0)
#dbox(550,1200,800,1600,0)
#dbox(1000,1200,1000,230,'s')
